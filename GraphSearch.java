import java.util.*;
import java.io.*;

public class GraphSearch {
	public static void main(String[] args)
	{
		try
		{
			if (args[0].equals("-p1"))
			{
				String filename = args[1];
				Reader input = new Reader();
				input.read(filename);
				Graph graph =  input.graph();
			}

		}

		catch (IOException e0)
		{
			System.err.println("Error parsing the graph file.");
			System.exit(1);
		}

	}
}